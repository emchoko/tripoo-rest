-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: tripoo_database
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alerts`
--

DROP TABLE IF EXISTS `alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alerts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `end_point` varchar(255) DEFAULT NULL,
  `start_point` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqx4kjyy8qmc38cpa1pj5gp74i` (`user_id`),
  CONSTRAINT `FKqx4kjyy8qmc38cpa1pj5gp74i` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alerts`
--

LOCK TABLES `alerts` WRITE;
/*!40000 ALTER TABLE `alerts` DISABLE KEYS */;
INSERT INTO `alerts` VALUES (2,'2017-02-28 22:53:53','Plovdiv','Pernik',9);
/*!40000 ALTER TABLE `alerts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES (1,'ROLE_USER'),(2,'ROLE_ADMIN');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `route_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKp9llkfdw32yg750n7eg2n01rp` (`route_id`),
  KEY `FKeyog2oic85xg7hsu2je2lx3s6` (`user_id`),
  CONSTRAINT `FKeyog2oic85xg7hsu2je2lx3s6` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FKp9llkfdw32yg750n7eg2n01rp` FOREIGN KEY (`route_id`) REFERENCES `stops` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookings`
--

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
INSERT INTO `bookings` VALUES (1,2,1),(2,1,4),(3,2,4),(4,36,11);
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comfort_level` int(11) NOT NULL,
  `manufacturer` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `photo_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqw4c8e6nqrvy3ti1xj8w8wyc9` (`user_id`),
  CONSTRAINT `FKqw4c8e6nqrvy3ti1xj8w8wyc9` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars`
--

LOCK TABLES `cars` WRITE;
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` VALUES (1,1,'Mazda','RX-8',2,1999,NULL),(2,5,'BMW','3',4,2013,NULL),(3,1,'mercedes','e',4,2013,NULL),(4,5,'Mercedes','E Class',4,2013,'http://images.autotrader.com/scaler/620/420/cms/images/cars/mercedes-benz/e/2014/2013-2014-e-class/209060.jpg'),(5,4,'bmw','m3',8,2005,NULL),(6,5,'mazda','6',9,2015,NULL);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `rating` int(11) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK28an517hrxtt2bsg93uefugrm` (`booking_id`),
  KEY `FKcgy7qjc1r99dp117y9en6lxye` (`user_id`),
  CONSTRAINT `FK28an517hrxtt2bsg93uefugrm` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`),
  CONSTRAINT `FKcgy7qjc1r99dp117y9en6lxye` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,4,'history of patch and',1,11);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stops`
--

DROP TABLE IF EXISTS `stops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stops` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `current_free_places` int(11) NOT NULL,
  `end_latitude` double NOT NULL,
  `end_point` varchar(255) NOT NULL,
  `end_time` datetime NOT NULL,
  `in_trip_id` bigint(20) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `start_latitude` double NOT NULL,
  `start_point` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `total_free_places` int(11) NOT NULL,
  `trip_id` bigint(20) DEFAULT NULL,
  `end_longitude` double NOT NULL,
  `start_longitude` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmf6n5nti6m0v6t36ap6qlps9a` (`trip_id`),
  CONSTRAINT `FKmf6n5nti6m0v6t36ap6qlps9a` FOREIGN KEY (`trip_id`) REFERENCES `trips` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stops`
--

LOCK TABLES `stops` WRITE;
/*!40000 ALTER TABLE `stops` DISABLE KEYS */;
INSERT INTO `stops` VALUES (1,3,12.13,'Plovdiv','2017-02-07 17:15:50',NULL,4,12.13,'Sofia','2017-02-07 16:15:50',4,3,0,0),(2,2,13.13,'Burgas','1970-01-01 17:15:00',NULL,4,12.13,'Plovdiv','1970-01-01 16:15:00',4,3,0,0),(3,4,14.13,'Varna','1970-01-01 17:15:00',NULL,4,12.13,'Burgas','1970-01-01 16:15:00',4,3,0,0),(4,4,12.13,'Plovdiv','1970-01-01 17:15:00',NULL,4,12.13,'Sofia','1970-01-01 16:15:00',4,NULL,0,0),(5,5,42.1354079,'Plovdiv','2017-01-18 18:20:33',NULL,4,42.697708199999994,'Sofia','2017-01-18 18:20:33',5,NULL,0,0),(6,5,42.1354079,'Plovdiv','2017-01-18 18:20:33',NULL,4,42.697708199999994,'Sofia','2017-01-18 18:20:33',5,NULL,0,0),(7,5,42.1354079,'Plovdiv','2017-01-18 18:20:53',NULL,4,42.697708199999994,'Sofia','2017-01-18 18:20:53',5,NULL,0,0),(8,5,42.4382418,'Ihtiman','2017-01-18 18:22:41',NULL,4,42.697708199999994,'Sofia','2017-01-18 18:22:41',5,NULL,0,0),(9,5,42.50479259999999,'Burgas','2017-01-18 18:22:41',NULL,4,42.4382418,'Ihtiman','2017-01-18 18:22:41',5,NULL,0,0),(10,5,42.1354079,'Plovdiv','2017-01-18 18:29:54',NULL,4,42.697708199999994,'Sofia','2017-01-18 18:29:54',5,NULL,0,0),(11,5,42.50479259999999,'Burgas','2017-01-18 18:29:54',NULL,4,42.1354079,'Plovdiv','2017-01-18 18:29:54',5,NULL,0,0),(12,5,42.1354079,'Plovdiv','2017-01-18 18:33:02',NULL,4,42.697708199999994,'Sofia','2017-01-18 18:33:02',5,NULL,0,0),(13,5,42.50479259999999,'Burgas','2017-01-18 18:33:02',NULL,4,42.1354079,'Plovdiv','2017-01-18 18:33:02',5,NULL,0,0),(14,5,42.697708199999994,'Sofia','2017-01-18 18:39:16',NULL,4,43.996159,'Vidin','2017-01-18 18:39:16',5,NULL,0,0),(15,5,41.8404241,'Bansko','2017-01-18 18:39:16',NULL,4,42.697708199999994,'Sofia','2017-01-18 18:39:16',5,NULL,0,0),(16,5,42.6022354,'Dragichevo','2017-01-18 18:47:38',NULL,4,42.697708199999994,'Sofia','2017-01-18 18:47:38',5,NULL,0,0),(17,5,42.6051862,'Pernik','2017-01-18 18:47:38',NULL,4,42.6022354,'Dragichevo','2017-01-18 18:47:38',5,NULL,0,0),(18,5,42.4382418,'Ihtiman','2017-01-18 18:50:43',NULL,4,42.697708199999994,'Sofia','2017-01-18 18:50:43',5,18,0,0),(19,5,42.1354079,'Plovdiv','2017-01-18 18:50:43',NULL,4,42.4382418,'Ihtiman','2017-01-18 18:50:43',5,18,0,0),(20,5,42.7031175,'Aytos','2017-01-18 22:53:03',NULL,5,42.1354079,'Plovdiv','2017-01-18 22:53:03',5,19,0,0),(21,5,43.2140504,'Varna','2017-01-18 22:53:03',NULL,7,42.7031175,'Aytos','2017-01-18 22:53:03',5,19,0,0),(22,5,42.697708199999994,'Sofia','2017-01-25 10:58:13',NULL,4,43.996159,'Vidin','2017-01-25 10:58:13',5,20,0,0),(23,5,42.50479259999999,'Burgas','2017-01-25 10:58:13',NULL,4,42.697708199999994,'Sofia','2017-01-25 10:58:13',5,20,0,0),(24,55,42.697708199999994,'Sofia','2017-01-25 11:29:01',NULL,3,43.996159,'Vidin','2017-01-25 11:29:01',55,21,0,0),(25,55,42.1354079,'Plovdiv','2017-01-25 11:29:01',NULL,4,42.697708199999994,'Sofia','2017-01-25 11:29:01',55,21,0,0),(26,55,42.50479259999999,'Burgas','2017-01-25 11:29:01',NULL,3,42.1354079,'Plovdiv','2017-01-25 11:29:01',55,21,0,0),(27,5,42.0208569,'Blagoevgrad','2017-01-27 11:51:03',NULL,5,42.697708199999994,'Sofia','2017-01-27 11:51:03',5,22,0,0),(28,5,51.3387609,'Krefeld','2017-01-27 11:51:03',NULL,0,42.0208569,'Blagoevgrad','2017-01-27 11:51:03',5,22,0,0),(29,5,41.8404241,'Bansko','2017-01-27 11:51:03',NULL,10,51.3387609,'Krefeld','2017-01-27 11:51:03',5,22,0,0),(30,5,42.50479259999999,'Burgas','2017-02-03 00:14:06',NULL,4,42.697708199999994,'Sofia','2017-02-03 00:14:06',5,25,27.462636099999997,23.321867500000003),(31,5,42.16955269999999,'Tsarevo','2017-02-03 00:14:06',NULL,4,42.50479259999999,'Burgas','2017-02-03 00:14:06',5,25,27.845415100000004,27.462636099999997),(32,5,42.681653600000004,'Sliven','2017-02-11 17:59:54',NULL,3,42.697708199999994,'Sofia','2017-02-11 17:59:54',5,26,26.3228685,23.321867500000003),(33,5,42.7031175,'Aytos','2017-02-11 17:59:54',NULL,7,42.681653600000004,'Sliven','2017-02-11 17:59:54',5,26,27.250643999999998,26.3228685),(34,5,42.50479259999999,'Burgas','2017-02-11 17:59:54',NULL,5,42.7031175,'Aytos','2017-02-11 17:59:54',5,26,27.462636099999997,27.250643999999998),(35,5,42.50479259999999,'Burgas','2017-02-11 18:05:04',NULL,0,42.697708199999994,'Sofia','2017-02-11 18:05:04',5,27,27.462636099999997,23.321867500000003),(36,4,41.5246053,'Melnik','2017-02-14 22:40:42',NULL,2,43.996159,'Vidin','2017-02-14 22:40:42',5,28,23.3915096,22.8679302);
/*!40000 ALTER TABLE `stops` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trips`
--

DROP TABLE IF EXISTS `trips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trips` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `car_id` bigint(20) DEFAULT NULL,
  `date` datetime NOT NULL,
  `luggage` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `trip_comment` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKnfmkeyk8b3gcia0l6h0dsnqg` (`car_id`),
  KEY `FK8wb14dx6ed0bpp3planbay88u` (`user_id`),
  CONSTRAINT `FK8wb14dx6ed0bpp3planbay88u` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FKnfmkeyk8b3gcia0l6h0dsnqg` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trips`
--

LOCK TABLES `trips` WRITE;
/*!40000 ALTER TABLE `trips` DISABLE KEYS */;
INSERT INTO `trips` VALUES (1,NULL,'2017-01-07 16:34:57',NULL,10,NULL,1),(2,NULL,'2017-01-07 16:37:50',0,4,NULL,2),(3,1,'2017-02-07 16:37:50',0,4,NULL,2),(4,NULL,'2017-01-18 00:32:09',1,5,'lalal',1),(5,NULL,'2017-01-16 21:50:11',0,5,' hegj',1),(6,NULL,'2017-01-18 00:57:51',1,5,'haha',1),(7,NULL,'2017-01-19 01:15:58',1,5,'FH is n USB',1),(8,NULL,'2017-01-18 01:19:22',1,5,'i will survive',1),(9,NULL,'2017-01-18 01:23:02',1,5,'enjoy',1),(10,NULL,'2017-02-07 16:37:50',0,4,'fsdfsdfsdfsdf',2),(11,NULL,'2017-01-18 18:19:42',1,50,'one more drink',1),(12,NULL,'2017-01-18 18:19:42',1,50,'one more drink',1),(13,NULL,'2017-01-25 18:21:57',0,555,'by',1),(14,NULL,'2017-01-18 18:29:18',1,58,'dick',1),(15,NULL,'2017-01-24 18:31:40',0,56,'Codigo',1),(16,NULL,'2017-01-18 18:38:21',0,8,'FH bulk',1),(17,NULL,'2017-01-18 18:46:55',0,5,'JFK',1),(18,NULL,'2017-01-18 18:49:54',0,655,'Chet',1),(19,NULL,'2017-01-18 22:52:10',0,10,'ruff',1),(20,NULL,'2017-01-25 10:57:33',0,7,'grid',4),(21,NULL,'2017-01-25 11:28:17',1,9,'did',4),(22,NULL,'2017-01-30 11:50:04',2,15,'high unto USCG rub',4),(25,NULL,'2017-02-22 00:13:24',0,8,'3rd',4),(26,NULL,'2017-02-23 17:58:58',0,15,'??????',4),(27,NULL,'2017-02-22 18:04:29',1,0,'brink and it will not only the new year',11),(28,NULL,'2017-02-22 22:40:03',0,4,'backchannel',9);
/*!40000 ALTER TABLE `trips` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_authorities`
--

DROP TABLE IF EXISTS `user_authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_authorities` (
  `user_id` bigint(20) NOT NULL,
  `authority_id` bigint(20) NOT NULL,
  KEY `FK6y0u41do0gynbgvlwnqngjudf` (`authority_id`),
  KEY `FKhiiib540jf74gksgb87oofni` (`user_id`),
  CONSTRAINT `FK6y0u41do0gynbgvlwnqngjudf` FOREIGN KEY (`authority_id`) REFERENCES `authorities` (`id`),
  CONSTRAINT `FKhiiib540jf74gksgb87oofni` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_authorities`
--

LOCK TABLES `user_authorities` WRITE;
/*!40000 ALTER TABLE `user_authorities` DISABLE KEYS */;
INSERT INTO `user_authorities` VALUES (1,1),(1,2),(2,1),(3,1),(4,1),(5,1),(7,1),(8,1),(9,1),(10,1),(11,1);
/*!40000 ALTER TABLE `user_authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `birth_date` datetime DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `last_password_reset` datetime DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `rating` double DEFAULT NULL,
  `username` varchar(32) NOT NULL,
  `photo_url` varchar(255) DEFAULT NULL,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_r43af9ap4edm43mmtq01oddj6` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,NULL,'admin@admin.com','',NULL,'admin',NULL,1,'admin',NULL,'','',NULL),(2,NULL,'enabled@user.com','',NULL,'pass',NULL,1,'user',NULL,'','',NULL),(3,NULL,'disabled@user.com','\0',NULL,'pass',NULL,1,'disabled',NULL,'','',NULL),(4,NULL,'pancho@user.com','',NULL,'$2a$10$.ZS/w854pQdg8lVF15T/PelTnsxuJQ2oT7wQVg0wlZKC79Rom.NKG',NULL,0,'pancho','http://i.imgur.com/ohLRU7c.jpg','','',NULL),(5,'1970-01-18 06:55:53','f@user.com','',NULL,'$2a$10$V7pDN9ov9jUMLM1k.PaXi.sBW.6LJoJet9..pCXAPsOB8WvUBENpO','0423842934',0,'test',NULL,'gag','gaga',NULL),(7,'2017-02-23 19:02:17','jcckcc@','',NULL,'$2a$10$ltaY7ExxFtpTqX1/gLt36e2M71Mdmi5I50x5q.SOP4qD3rtCHYcqK','8 886868686868',0,'cushhj',NULL,'kvcjccj','jcifjfjc',NULL),(8,'1994-02-25 19:08:02','emc@bh','',NULL,'$2a$10$3cpqPC2XAGonriUw0Tkk1uRgiupuwN0oGR.BOLyJUf8TJTjsQZZsO','5655688',0,'gosho',NULL,'gosho','petrov',NULL),(9,'2017-02-25 20:30:28','egy@gmail.com','',NULL,'$2a$10$.Zv3sJos3DoWGNrm6LQIquvgOjgACWRFKmKmJcncNyJwiOCfLjNr.','08665588484',1,'jorolubimeca','http://i.imgur.com/zcHfBgy.jpg','Joro','Lubimeca','cas4pH-jtZs:APA91bGyKojNamYVWao-N60A9oCd5ZzU_FEWttIGmEhG82-Shiz1CvWpib4nJQX82Gfyyl1T9j2cgfWro4mBjz-NbpaPTNuwS5XrFN2dJqreTmGuzYUnE18DHBIrTaK5Rri0gWvK_bxo'),(10,'2017-02-08 21:38:18','emc@bh','',NULL,'$2a$10$lVRAt1oaYkfQJdlvEqWLVO4qZbKG92vnKFxfrj8g/pegDzFWhL7Tm','96 682800250',0,'daddy',NULL,'  gvgvg y','hcycch',NULL),(11,'2017-02-22 18:03:21','efjfsd@jkl','',NULL,'$2a$10$Vz6T/KcUnqolJMU3dA3ubuxvxurW4FDisXFNcksFsriosMMab6BS6','95888558',0,'emillozev','http://i.imgur.com/w3FO0WS.jpg','Emil','Lozev','d0Uu_PUnWOY:APA91bFoadze8-4HrTRB3ikLAKD3lQWSRlvpdOQeIFf3QxGJOSUXKpJ_S8yc9FJmBDl-XKlIXGHu8qsqLlQ8N29k2CaOe7wTUHr7WA1qb1jElDLUTV9cQLkcoBwW01maqBEzNwWaZPKs');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-02 23:41:43
