INSERT INTO users (id, username, password, first_name, last_name, email, enabled, rating) VALUES (1, 'admin', 'admin', 'emil', 'lozev', 'admin@admin.com', 1, 1);
INSERT INTO users (id, username, password, first_name, last_name, email, enabled, rating) VALUES (2, 'user', 'pass', 'user', 'user', 'enabled@user.com', 1, 1);
INSERT INTO users (id, username, password, first_name, last_name, email, enabled, rating) VALUES (3, 'disabled', 'pass', 'user', 'user', 'disabled@user.com', 0, 1);

INSERT INTO authorities (id, name) VALUES (1, 'ROLE_USER');
INSERT INTO authorities (id, name) VALUES (2, 'ROLE_ADMIN');

INSERT INTO user_authorities (user_id, authority_id) VALUES (1, 1);
INSERT INTO user_authorities (user_id, authority_id) VALUES (1, 2);
INSERT INTO user_authorities (user_id, authority_id) VALUES (2, 1);
INSERT INTO user_authorities (user_id, authority_id) VALUES (3, 1);

INSERT INTO trips (id, price, user_id, date) VALUES (1, 10, 1, NOW());
