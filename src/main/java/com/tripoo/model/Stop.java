package com.tripoo.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by emil on 3/20/17.
 */

@Entity
@Table(name = "stops")
public class Stop {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "latitude", nullable = false)
    private double latitude;

    @Column(name = "longitude", nullable = false)
    private double longitude;

    @Column(name = "price", nullable = false)
    private int price;

    @Column(name = "in_trip_id", nullable = false)
    private int inTripId;

    @Column(name = "time", nullable = false)
    private Date time;

    @Column(name = "seats", nullable = false)
    private int seats;

    @Column(name = "startSeats", nullable = false)
    private int startSeats;

    @Column(name = "trip_id")
    private Long tripId;

    @ManyToOne
    @JoinColumn(name = "trip_id", updatable = false, insertable = false)
    private Trip trip;

    @OneToMany(mappedBy = "stop", cascade = CascadeType.ALL, targetEntity = Booking.class)
    private List<Booking> bookings;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Stop stop = (Stop) o;

        return name != null ? name.equals(stop.name) : stop.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    public Stop() {
    }

    public Long getTripId() {
        return tripId;
    }

    public void setTripId(Long tripId) {
        this.tripId = tripId;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getInTripId() {
        return inTripId;
    }

    public void setInTripId(int inTripId) {
        this.inTripId = inTripId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getStartSeats() {
        return startSeats;
    }

    public void setStartSeats(int startSeats) {
        this.startSeats = startSeats;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }
}
