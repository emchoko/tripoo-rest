package com.tripoo.model.security;

/**
 * Created by emil on 11/15/16.
 */
public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}
