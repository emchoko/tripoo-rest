package com.tripoo.model.security;

import java.io.Serializable;

/**
 * Created by emil on 11/15/16.
 */
public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 5037563290670768365L;

    private final String token;

    public JwtAuthenticationResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
