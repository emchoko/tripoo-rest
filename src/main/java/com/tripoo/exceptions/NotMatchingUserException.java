package com.tripoo.exceptions;

import org.aspectj.weaver.ast.Not;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by emil on 11/22/16.
 */
@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class NotMatchingUserException extends RuntimeException {

    public NotMatchingUserException(){
        super();
    }

    public NotMatchingUserException(String message){
        super(message);
    }
}
