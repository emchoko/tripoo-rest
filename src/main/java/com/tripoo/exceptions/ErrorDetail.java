package com.tripoo.exceptions;

/**
 * Created by emil on 12/7/16.
 */
public class ErrorDetail {

    private int status;

    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
