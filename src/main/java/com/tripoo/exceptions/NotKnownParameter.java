package com.tripoo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by emil on 1/24/17.
 */
@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class NotKnownParameter extends RuntimeException {
    public NotKnownParameter() {
        super();
    }

    public NotKnownParameter(String message) {
        super(message);
    }
}
