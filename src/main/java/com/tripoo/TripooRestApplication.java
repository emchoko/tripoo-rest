package com.tripoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class TripooRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TripooRestApplication.class, args);
    }

}
