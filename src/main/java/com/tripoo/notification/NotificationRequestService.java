package com.tripoo.notification;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by emil on 2/14/17.
 */
public interface NotificationRequestService {

    @Headers({
            "Content-Type: application/json",
    })
    @POST("send")
    Call<Void> sendNotificationToDevice(@Body NotificationBody body, @Header("Authorization") String serverKey);
}
