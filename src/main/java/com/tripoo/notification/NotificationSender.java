package com.tripoo.notification;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by emil on 2/14/17.
 */
public class NotificationSender {

    private static final String TAG = NotificationSender.class.getSimpleName();

    private static final String BASE_URL = "https://fcm.googleapis.com/fcm/";

    private static Retrofit retrofit;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
