package com.tripoo.config;

import com.tripoo.model.Trip;
import com.tripoo.service.interfaces.TripService;
import com.tripoo.util.AlertChecker;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by emil on 12/29/16.
 */
@Component
public class AlertTimer {

    private static final Logger logger = Logger.getLogger(AlertChecker.class.getName());

    @Autowired
    private TripService tripService;

    @Async
    @Transactional
    @Scheduled(fixedDelay = 5000000)
    public void checkAlerts(){
        logger.info("AlertTimer: checkAlerts(): before if");
//        for (Trip trip : tripService.findAll()) {
//            logger.info("AlertTimer: checkAlerts(): in if");
//            tripService.alertChecker(trip);
//        }
    }
}
