package com.tripoo.config;

import com.tripoo.dto.*;
import com.tripoo.dto.list.*;
import com.tripoo.model.*;
import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by emil on 12/11/16.
 */
@Configuration
public class DozerConfig {
    @Bean(name = "org.dozer.Mapper")
    public DozerBeanMapper dozerBeanMapper() {
        DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();

        dozerBeanMapper.addMapping(new TripDTOMapperConfig());
        dozerBeanMapper.addMapping(new TripListMapperConfig());

        dozerBeanMapper.addMapping(new UserDTOMapperConfig());
        dozerBeanMapper.addMapping(new UserListMapperConfig());
        dozerBeanMapper.addMapping(new UserProfileDTOConfig());
        dozerBeanMapper.addMapping(new UserIdAndUsernameConfig());
        dozerBeanMapper.addMapping(new UserAllConfig());

        dozerBeanMapper.addMapping(new StopDTOMapperConfig());
        dozerBeanMapper.addMapping(new StopListMapperConfig());

        dozerBeanMapper.addMapping(new CarDTOMapperConfig());

        dozerBeanMapper.addMapping(new ReviewListDTOConfig());
        dozerBeanMapper.addMapping(new ReviewDTOConfig());

        dozerBeanMapper.addMapping(new AlertListDTOConfig());

        dozerBeanMapper.addMapping(new BookingDTOConfig());
        dozerBeanMapper.addMapping(new BookingListDTOConfig());

        return dozerBeanMapper;
    }

    class BookingListDTOConfig extends BeanMappingBuilder{

        @Override
        protected void configure() {
            mapping(Booking.class, BookingListDTO.class);
        }
    }

    class BookingDTOConfig extends BeanMappingBuilder {

        @Override
        protected void configure() {
            mapping(Booking.class, BookingDTO.class);
        }
    }

    class UserAllConfig extends BeanMappingBuilder {
        @Override
        protected void configure() {
            mapping(User.class, com.tripoo.dto.all.UserAll.class);
        }
    }

    class UserIdAndUsernameConfig extends BeanMappingBuilder {
        @Override
        protected void configure() {
            mapping(User.class, UserIdAndUsername.class);
        }
    }

    class AlertListDTOConfig extends BeanMappingBuilder {
        @Override
        protected void configure() {
            mapping(Alert.class, AlertListDTO.class);
        }
    }

    class ReviewDTOConfig extends BeanMappingBuilder {
        @Override
        protected void configure() {
            mapping(Review.class, ReviewDTO.class);
        }
    }

    class ReviewListDTOConfig extends BeanMappingBuilder {
        @Override
        protected void configure() {
            mapping(Review.class, ReviewListDTO.class);
        }
    }

    class UserProfileDTOConfig extends BeanMappingBuilder {
        @Override
        protected void configure() {
            mapping(User.class, UserProfileDTO.class);
        }
    }

    class TripDTOMapperConfig extends BeanMappingBuilder {
        @Override
        protected void configure() {
            mapping(Trip.class, TripDTO.class);
        }
    }

    class UserDTOMapperConfig extends BeanMappingBuilder {

        @Override
        protected void configure() {
            mapping(User.class, UserDTO.class);
        }
    }



    class CarDTOMapperConfig extends BeanMappingBuilder {

        @Override
        protected void configure() {
            mapping(Car.class, CarDTO.class);
        }
    }

    class TripListMapperConfig extends BeanMappingBuilder {

        @Override
        protected void configure() {
            mapping(Trip.class, TripListDTO.class);
        }
    }

    class StopListMapperConfig extends BeanMappingBuilder {

        @Override
        protected void configure() {
            mapping(Stop.class, StopListDTO.class);
        }
    }

    class StopDTOMapperConfig extends BeanMappingBuilder {

        @Override
        protected void configure() {
            mapping(Stop.class, StopDTO.class);
        }
    }

    class UserListMapperConfig extends BeanMappingBuilder {

        @Override
        protected void configure() {
            mapping(User.class, UserListDTO.class);
        }
    }
}