package com.tripoo.service.implementation;

import com.tripoo.dao.BookingRepository;
import com.tripoo.dao.StopRepository;
import com.tripoo.exceptions.NotKnownParameter;
import com.tripoo.exceptions.NotMatchingUserException;
import com.tripoo.model.Booking;
import com.tripoo.model.Stop;
import com.tripoo.model.security.JwtUser;
import com.tripoo.service.interfaces.StopService;
import com.tripoo.util.CheckCurrentUserId;
import com.tripoo.util.Constants;
import com.tripoo.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emil on 3/20/17.
 */
@Service
public class StopServiceImpl implements StopService {

    @Autowired
    private StopRepository stopRepository;

    //TODO: ask if this is good to do
    @Autowired
    private BookingRepository bookingRepository;

    @Override
    public List<Stop> findAll(int page) {
        List<Stop> res = new ArrayList<>();
        stopRepository.findAll(new PageRequest(page, Constants.DEFAULT_PAGE_SIZE)).forEach(res::add);
        return res;
    }

    @Override
    public Stop findOne(Long id) {
        return stopRepository.findOne(id);
    }

    @Override
    public Stop save(Stop stop) {
        //TODO: add trip id
        return stopRepository.save(stop);
    }

    @Override
    public void delete(Long id) {
        //TODO: check the user credentials
    }

    @Override
    public Stop update(Long id, Stop stop) {
        //TODO: check the user credentials
        //TODO: update with new values
        stopRepository.findOne(id);
        return stopRepository.save(stop);
    }


    //TODO: fix for many to many relation
    @Override
    public void book(Long stopId, Booking booking) {
        Stop stopToBook = stopRepository.findOne(stopId);
        if(!CheckCurrentUserId.check(stopToBook.getTrip().getUserId())){
            JwtUser user = UserContext.getUser();
            assert user != null;
            booking.setUserId(user.getId());
            bookSeats(booking.getPlaces(), stopToBook);
            bookingRepository.save(booking);
        } else throw new NotMatchingUserException("Cannot book own trip");
    }

    private void bookSeats(int places, Stop stop){
        int remainingSeats = stop.getSeats() - places;
        if(remainingSeats < 0)
            throw new NotKnownParameter("Only " + stop.getSeats() + " available");

        stop.setSeats(remainingSeats);
        stopRepository.save(stop);
    }


}
