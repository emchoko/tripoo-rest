package com.tripoo.service.implementation;

import com.tripoo.dao.AuthorityRepository;
import com.tripoo.model.security.Authority;
import com.tripoo.service.interfaces.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by emil on 11/18/16.
 */

@Service
public class AuthorityServiceImpl implements AuthorityService {

    @Autowired
    private AuthorityRepository authorityRepository;

    @Override
    public Iterable<Authority> findAll() {
        return authorityRepository.findAll();
    }

    @Override
    public Authority findOne(Long id) {
        return authorityRepository.findOne(id);
    }

    @Override
    public Authority save(Authority authority) {
        return authorityRepository.save(authority);
    }

    @Override
    public void delete(Authority id) {
        authorityRepository.delete(id);
    }
}
