package com.tripoo.service.implementation;

import com.tripoo.controller.TripRestController;
import com.tripoo.dao.BookingRepository;
import com.tripoo.dao.StopRepository;
import com.tripoo.dao.UserRepository;
import com.tripoo.exceptions.NotMatchingUserException;
import com.tripoo.model.*;
import com.tripoo.model.security.JwtUser;
import com.tripoo.service.interfaces.BookingService;
import com.tripoo.service.interfaces.ReviewService;
import com.tripoo.util.CheckCurrentUserId;
import com.tripoo.util.UserContext;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Created by emil on 12/5/16.
 */
@Service
public class BookingServiceImpl implements BookingService {

    private static Logger logger = Logger.getLogger(TripRestController.class.getName());

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private StopRepository stopRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Iterable<Booking> findAll() {
        return bookingRepository.findAll();
    }

    @Override
    public Booking findOne(Long id) {
        return bookingRepository.findOne(id);
    }

    @Override
    public Booking save(Booking booking) {
        return bookingRepository.save(booking);
    }

    @Override
    public void delete(Long id) {
        if (CheckCurrentUserId.check(bookingRepository.findOne(id).getUser().getId())) {
            Booking booking = bookingRepository.findOne(id);
            Stop stop = booking.getStop();
            stop.setSeats(stop.getSeats() + booking.getPlaces());
            stopRepository.save(stop);
            bookingRepository.delete(id);
        } else throw new NotMatchingUserException("User with id: " + UserContext.getUser().getId() + " has not created review with id: " + id);
    }

    @Override
    public Booking update(Long id, Booking booking) {
        if (CheckCurrentUserId.check(bookingRepository.findOne(id).getUserId())) {
            booking.setId(id);
            return bookingRepository.save(booking);
        } else {
            throw new NotMatchingUserException("User with id: " + UserContext.getUser().getId() + " has not created booking with id: " + id);
        }
    }

    @Override
    public Review addReview(Long id, Review review) {
        if (checkIfUserHasReviewedRoute(id)) {
            review.setBookingId(id);
            addRatingToUser(id, review.getRating());
            return reviewService.save(review);
        } else {
            throw new NotMatchingUserException("Limit of one review per booking!");
        }
    }

    private boolean checkIfUserHasReviewedRoute(Long bookingId) {
        JwtUser user = UserContext.getUser();
        return !(bookingRepository.findOne(bookingId).getReview() != null &&
                Objects.equals(bookingRepository.findOne(bookingId).getUserId(), user.getId()));
    }

    private void addRatingToUser(long bookingId, int rating) {
        User user = bookingRepository.findOne(bookingId).getStop().getTrip().getUser();
        int currentReviews = 0;
        for (Trip trip : user.getTrips()) {
            for (Stop stop : trip.getStops()) {
                for (Booking booking : stop.getBookings()) {
                    if (booking.getReview() != null) {
                        logger.info("Trip: " + trip.getId() + ":\n  Route: " + stop.getId() + ": \n" + "Bookings Review Ratings: " + booking.getReview().getRating());
                        currentReviews++;
                    }
                }
            }
        }

        double newUserRating = (user.getRating() * currentReviews + rating) / ++currentReviews;
        logger.info("BookingServiceImpl: " + newUserRating);
        user.setRating(newUserRating);
        userRepository.save(user);
    }

}
