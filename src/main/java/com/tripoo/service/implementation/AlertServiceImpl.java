package com.tripoo.service.implementation;

import com.tripoo.dao.AlertRepository;
import com.tripoo.exceptions.NotMatchingUserException;
import com.tripoo.model.Alert;
import com.tripoo.service.interfaces.AlertService;
import com.tripoo.util.CheckCurrentUserId;
import com.tripoo.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by emil on 12/28/16.
 */
@Service
public class AlertServiceImpl implements AlertService {

    @Autowired
    private AlertRepository alertRepository;

    @Override
    public List<Alert> findAll() {
        return alertRepository.findAll();
    }

    @Override
    public Alert findOne(Long id) {
        return alertRepository.findOne(id);
    }

    @Override
    public Alert save(Alert alert) {
        if (alert.getUserId() == null)
            alert.setUserId(UserContext.getUser().getId());
        return alertRepository.save(alert);
    }

    @Override
    public void delete(Long id) {
        Long userId = alertRepository.findOne(id).getUserId();
        if (CheckCurrentUserId.check(userId)) {
            alertRepository.delete(id);
        } else {
            throw new NotMatchingUserException("User with id: " + userId + " has no permissions on alert with id: " + id);
        }
    }

    @Override
    public Alert update(Long id, Alert alert) {
        Long userId = alertRepository.findOne(id).getUserId();
        if(CheckCurrentUserId.check(userId)){
            alert.setId(id);
            return alertRepository.save(alert);
        }else{
            throw new NotMatchingUserException("User with id: " + userId + " has no permissions on alert with id: " + id);

        }
    }


}
