package com.tripoo.service.implementation;

import com.tripoo.dao.AlertRepository;
import com.tripoo.dao.StopRepository;
import com.tripoo.dao.TripRepository;
import com.tripoo.exceptions.NotMatchingUserException;
import com.tripoo.model.Alert;
import com.tripoo.model.Stop;
import com.tripoo.model.Trip;
import com.tripoo.model.security.JwtUser;
import com.tripoo.notification.Notification;
import com.tripoo.notification.NotificationBody;
import com.tripoo.notification.NotificationRequestService;
import com.tripoo.notification.NotificationSender;
import com.tripoo.service.interfaces.TripService;
import com.tripoo.util.CheckCurrentUserId;
import com.tripoo.util.Constants;
import com.tripoo.util.Mapper;
import com.tripoo.util.UserContext;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by emil on 11/10/16.
 */
@Service
public class TripServiceImpl implements TripService {

    private static Logger logger = Logger.getLogger(TripServiceImpl.class.getName());

    @Autowired
    private static Mapper mapper;

    @Autowired
    private TripRepository tripRepository;

    @Autowired
    private AlertRepository alertRepository;

    @Autowired
    private StopRepository stopRepository;


    private static List<Trip> tripMapper(List<Trip> notMappedTrips, String mapping) {
        return mapper.mapTrips(notMappedTrips, mapping);
    }

    @Override
    public List findAll(int page) {
        PageRequest pageRequest = new PageRequest(page, Constants.DEFAULT_PAGE_SIZE);

        List<Trip> result = new ArrayList<>();

        tripRepository.findAll(pageRequest).forEach(result::add);
        return result;
    }

    @Override
    public Trip findOne(Long id) {
        Trip one = tripRepository.findOne(id);
        return one;
    }

    @Override
    public Trip save(Trip trip) {
        trip.setUserId(UserContext.getUser().getId());
        Trip svdTrip = tripRepository.save(trip);
        trip.getStops().forEach(stop -> {
            stop.setTripId(svdTrip.getId());
            stopRepository.save(stop);
            logger.info("stop: " + stop.getId());
        });

        return tripRepository.save(trip);
    }

    @Override
    public void alertChecker(Trip trip) {
        for (Alert alert : alertRepository.findAll()) {
            logger.info("checking");
            if (stopChecker(trip, alert.getStartPoint(), alert.getEndPoint()) && trip.getDate().after(alert.getDate()) && alert.getUserId() != trip.getUserId()) {
                sendNotification("Tripoo Alert!", "We found an trip you were looking for!", "none", alert.getUser().getToken());

                logger.info("alertChecker(): SEND ALERT TO USER WITH ID:" + alert.getUserId());
                alertRepository.delete(alert.getId());
            }
        }

    }

    private void sendNotification(String title, String body, String icon, String token) {
        NotificationRequestService service = NotificationSender.getClient().create(NotificationRequestService.class);

        Notification notification = new Notification();
        notification.setTitle(title);
        notification.setBody(body);
        notification.setIcon(icon);

        NotificationBody notificationBody = new NotificationBody();
        notificationBody.setNotification(notification);
        notificationBody.setTo(token);

        Call<Void> call = service.sendNotificationToDevice(notificationBody, Constants.FIREBASE_SERVER_KEY);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                logger.info(call.request().body());
                logger.info(response.code() + "\n" + response.message() + "\n" + response.raw() + "\n" + response.errorBody());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                logger.info("Failure sending notification! " + t.toString());
            }
        });
    }

    //KEEP ROUTES
    @Override
    public Trip update(Long id, Trip trip) {

        if (CheckCurrentUserId.check(trip.getUserId())) {
            Trip tripUpdate = tripRepository.findOne(id);
            tripUpdate.setDate(trip.getDate());
            tripUpdate.setStops(trip.getStops());
            tripUpdate.setPrice(trip.getPrice());
            return tripRepository.save(tripUpdate);
        } else {
            throw new NotMatchingUserException("Trip: User with id: " + UserContext.getUser().getId() + " has not created trip with id: " + id);
        }
    }

    /**
     * Finds trip with routes that have the same startPoint, endPoint and date after the given.
     * If parameters are null, all the trips are returned
     *
     * @param startPoint - start point
     * @param endPoint   - end point
     * @param date       - date after which trips are returned
     * @param page
     * @return - List of trips
     */
    //TODO: remove this method and replace it with separate ones
    @Override
    public List findWithParams(String startPoint, String endPoint, Long date, int page) {

        if (startPoint == null || endPoint == null || date == null)
            return (List<Trip>) findAll(page);

        Date convertedDate;
        if (date == 0)
            convertedDate = new Date();
        else
            convertedDate = new Date(date);

        logger.info("TripServiceImpl: findWithParams(): date : " + convertedDate);

        return tripSelector(findAll(page), startPoint, endPoint, convertedDate);
    }

    @Override
    public void delete(Long id) {
        if (CheckCurrentUserId.check(tripRepository.findOne(id).getUserId())) {
            tripRepository.delete(id);
        } else {
            // throw Exception..
            throw new NotMatchingUserException("Trip: User with id: " + UserContext.getUser().getId() + " has not created trip with id: " + id);
        }
    }

    @Override
    public Trip addCarToTrip(Long tripId, Long carId) {
        Trip trip = findOne(tripId);
        if (CheckCurrentUserId.check(trip.getUser().getId())) {
            trip.setCarId(carId);
            return save(trip);
        } else {
            throw new NotMatchingUserException("Trip: User with id: " + UserContext.getUser().getId() + " has not created trip with id: " + tripId);
        }
    }


    @Override
    public Trip saveTripToUser(Trip trip) {
        JwtUser user = UserContext.getUser();

        trip.setUserId(user.getId());

//        Trip savedTrip = tripRepository.save(trip);
//        Long tripId = savedTrip.getId();
//        if (savedTrip.getStops() != null && savedTrip.getStops().size() > 0) {
//            for (Route route : savedTrip.getStops()) {
//                route.setTripId(tripId);
//                routeRepository.save(route);
//            }
//        }

        Trip svdTrip = tripRepository.save(trip);
        trip.getStops().forEach(stop -> {
//            stop.setTripId(svdTrip.getId());
//            stopRepository.save(stop);
            logger.info("stop trip id: " + stop.getTripId());
        });

        return svdTrip;
//        return tripRepository.save(trip);
    }

    private List<Trip> tripSelector(List<Trip> trips, String startPoint, String endPoint, Date date) {
        List<Trip> matchingTrips = new ArrayList<>();
        for (Trip trip : trips) {
            if (trip.getDate().after(date) && stopChecker(trip, startPoint, endPoint)) {
                matchingTrips.add(trip);
            }
        }
        return matchingTrips;
    }

    private boolean stopChecker(Trip trip, String startPoint, String endPoint) {
        int cnt = 0;

        for(Stop stop: trip.getStops()){
            if(startPoint.equals(stop.getName()) || endPoint.equals(stop.getName()))
                cnt++;
        }

        return cnt == 2;
    }

}
