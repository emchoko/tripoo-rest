package com.tripoo.service.implementation;

import com.tripoo.dao.CarRepository;
import com.tripoo.exceptions.NotMatchingUserException;
import com.tripoo.model.Car;
import com.tripoo.model.security.JwtUser;
import com.tripoo.service.interfaces.CarService;
import com.tripoo.util.CheckCurrentUserId;
import com.tripoo.util.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by emil on 12/6/16. CarServiceImpl
 */
@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;

    @Override
    public Iterable<Car> findAll() {
        return carRepository.findAll();
    }

    @Override
    public Car findOne(Long id) {
        return carRepository.findOne(id);
    }

    @Override
    public Car save(Car car) {
        JwtUser user = UserContext.getUser();
        assert user != null;
        car.setUserId(user.getId());
        return carRepository.save(car);
    }

    @Override
    public void delete(Long id) {
        if (CheckCurrentUserId.check(findOne(id).getUserId())) {
            carRepository.delete(id);
        } else {
            throw new NotMatchingUserException("Car: User(" + findOne(id).getUser().getId() + ") has not created this car(" + findOne(id).getId() + ")!");
        }
    }

    @Override
    public Car update(Long id, Car car) {
        if (CheckCurrentUserId.check(car.getUserId())) {
            car.setId(id);
            return save(car);
        } else {
            throw new NotMatchingUserException("Car: User(" + findOne(id).getUser().getId() + ") has not created this car(" + findOne(id).getId() + ")!");
        }
    }

    @Override
    public Car saveCarToUser(Long userId, Car car) {
        if (CheckCurrentUserId.check(userId)) {
            car.setUserId(userId);
            return save(car);
        } else {
            throw new NotMatchingUserException("Car: Can add cars only to our profile!");
        }
    }


}
