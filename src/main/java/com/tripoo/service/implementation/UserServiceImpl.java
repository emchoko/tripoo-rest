package com.tripoo.service.implementation;

import com.tripoo.dao.UserRepository;
import com.tripoo.exceptions.NotMatchingUserException;
import com.tripoo.model.Booking;
import com.tripoo.model.Trip;
import com.tripoo.model.User;
import com.tripoo.model.security.Authority;
import com.tripoo.model.security.JwtUser;
import com.tripoo.service.interfaces.AuthorityService;
import com.tripoo.service.interfaces.UserService;
import com.tripoo.util.CheckCurrentUserId;
import com.tripoo.util.UserContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by emil on 11/10/16.
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Log logger = LogFactory.getLog(UserServiceImpl.class.getClass());

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityService authorityService;

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findOne(Long id) {
        return userRepository.findOne(id);
    }


    @Override
    public User save(User user) {
        List<Authority> authorityList = new ArrayList<>(1);
        authorityList.add(authorityService.findOne((long) 1));
        logger.debug("\n \n ROLE_USER: " + authorityService.findOne((long) 1).getName() + "\n");
        user.setAuthorities(authorityList);
        user.setEnabled(true);
        user.setRating(1);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    //TODO: reimplement
    @Override
    public User update(Long id, User user) {
        JwtUser loggedUser = UserContext.getUser();
        if (CheckCurrentUserId.check(userRepository.findOne(id).getId())) {
            User userUpdate = findOne(id);
            userUpdate.setUsername(user.getUsername());
            userUpdate.setPassword(user.getPassword());
            userUpdate.setFirstName(user.getFirstName());
            userUpdate.setLastName(user.getLastName());
            userUpdate.setTrips(user.getTrips());
            userUpdate.setRating(user.getRating());
            return userRepository.save(userUpdate);
        } else {
            throw new NotMatchingUserException("User: User with id: " + loggedUser.getId() + " has no credentials to user with id: " + id);
        }
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void delete(Long id) {
        userRepository.delete(id);
    }


    /**
     * getUserBookedTrips - return all the bookings after date now
     *
     * @param userId - user id to get bookings from
     * @return List of Trips
     */
    @Override
    public List<Trip> getUserBookedTrips(Long userId) {
        List<Booking> bookings = userRepository.findOne(userId).getBookings();
        List<Trip> bookedTrips = new ArrayList<>();
        Date nowDate = new Date();
        for (Booking booking : bookings) {
            Trip trip = booking.getStop().getTrip();
//        TODO: Uncomment the following line
            //    if(trip.getDate().after(nowDate))
            bookedTrips.add(trip);
            logger.info("getUserBookedTrips(): " + trip.getId());
        }

        bookedTrips = bookedTrips.stream().distinct().collect(Collectors.toList());

        return bookedTrips;
    }

    @Override
    public List<Trip> findUserTrips(Long id, Long date) {
        if (date == null) {
            return findOne(id).getTrips();
        } else {
            Date convertedDate = new Date(date);
            return findOne(id).getTrips().stream().filter(trip -> trip.getDate().after(convertedDate)).collect(Collectors.toList());
        }
    }

    //TODO: fix the duplicate code
    @Override
    public void updatePhotoUrl(Long userId, String photoUrl) {
        JwtUser loggedUser = UserContext.getUser();
        if (CheckCurrentUserId.check(userRepository.findOne(userId).getId())) {
            User userUpdate = findOne(userId);
            userUpdate.setPhotoUrl(photoUrl);
            userRepository.save(userUpdate);
        } else {
            throw new NotMatchingUserException("User: User with id: " + loggedUser.getId() + " has no credentials to user with id: " + userId);
        }
    }

    @Override
    public void updateToken(Long userId, String token) {
        JwtUser loggedUser = UserContext.getUser();
        if (CheckCurrentUserId.check(userRepository.findOne(userId).getId())) {
            User userUpdate = findOne(userId);
            userUpdate.setToken(token);
            userRepository.save(userUpdate);
        } else {
            throw new NotMatchingUserException("User: User with id: " + loggedUser.getId() + " has no credentials to user with id: " + userId);
        }
    }

}