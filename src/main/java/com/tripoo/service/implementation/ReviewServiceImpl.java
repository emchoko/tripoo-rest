package com.tripoo.service.implementation;

import com.tripoo.dao.ReviewRepository;
import com.tripoo.dao.UserRepository;
import com.tripoo.exceptions.NotMatchingUserException;
import com.tripoo.model.Review;
import com.tripoo.model.security.JwtUser;
import com.tripoo.service.interfaces.ReviewService;
import com.tripoo.util.CheckCurrentUserId;
import com.tripoo.util.UserContext;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by emil on 11/10/16.
 */
@Service
public class ReviewServiceImpl implements ReviewService {

    private static Logger logger = Logger.getLogger(ReviewService.class.getName());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Override
    public Iterable<Review> findAll() {
        return reviewRepository.findAll();
    }

    @Override
    public Review findOne(Long id) {
        return reviewRepository.findOne(id);
    }

    @Override
    public Review save(Review review) {
        JwtUser user = UserContext.getUser();
        assert user != null;
        review.setUser(userRepository.findByUsername(user.getUsername()));
        return reviewRepository.save(review);
    }

    @Override
    public void delete(Long id) {
        logger.debug("In here");
        if (CheckCurrentUserId.check(reviewRepository.findOne(id).getUser().getId())) {
            logger.debug("In here");
            reviewRepository.delete(id);
        } else {
            throw new NotMatchingUserException("User with id: " + UserContext.getUser().getId() + " has not created review with id: " + id);
        }
    }

    @Override
    public Review update(Long id, Review review) {
        if (CheckCurrentUserId.check(reviewRepository.findOne(id).getUser().getId())) {
            review.setId(id);
            return reviewRepository.save(review);
        } else {
            throw new NotMatchingUserException("User with id: " + UserContext.getUser().getId() + " has not created review with id: " + id);
        }
    }
}
