package com.tripoo.service.interfaces;

import com.tripoo.model.Trip;
import com.tripoo.model.User;

import java.util.List;

/**
 * Created by emil on 11/10/16.
 */
public interface UserService {
    Iterable<User> findAll();

    User findOne(Long id);

    User save(User user);

    User update(Long id, User user);

    User findByUsername(String username);

    void delete(Long id);

    List<Trip> getUserBookedTrips(Long userId);

    List<Trip> findUserTrips(Long id,Long date);

    void updatePhotoUrl(Long userId, String photoUrl);

    void updateToken(Long userId, String token);
}
