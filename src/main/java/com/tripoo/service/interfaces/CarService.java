package com.tripoo.service.interfaces;

import com.tripoo.model.Car;

/**
 * Created by emil on 12/6/16.
 */
public interface CarService {
    Iterable<Car> findAll();

    Car findOne(Long id);

    Car save(Car car);

    void delete(Long id);

    Car update(Long id, Car car);

    Car saveCarToUser(Long userId, Car car);
}
