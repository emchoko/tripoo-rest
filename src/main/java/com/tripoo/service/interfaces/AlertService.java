package com.tripoo.service.interfaces;

import com.tripoo.model.Alert;

import java.util.List;

/**
 * Created by emil on 12/28/16.
 */
public interface AlertService {
    List<Alert> findAll();

    Alert findOne(Long id);

    Alert save(Alert alert);

    void delete(Long id);

    Alert update(Long id, Alert alert);
}
