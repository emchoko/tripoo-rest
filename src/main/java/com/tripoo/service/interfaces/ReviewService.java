package com.tripoo.service.interfaces;

import com.tripoo.model.Review;

/**
 * Created by emil on 11/10/16.
 */
public interface ReviewService {
    Iterable<Review> findAll();

    Review findOne(Long id);

    Review save(Review review);

    void delete(Long id);

    Review update(Long id, Review review);
}