package com.tripoo.service.interfaces;

import com.tripoo.model.security.Authority;

/**
 * Created by emil on 11/18/16.
 */
public interface AuthorityService {
    Iterable<Authority> findAll();

    Authority findOne(Long id);

    Authority save(Authority authority);

    void delete(Authority id);
}
