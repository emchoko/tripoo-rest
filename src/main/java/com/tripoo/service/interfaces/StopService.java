package com.tripoo.service.interfaces;

import com.tripoo.model.Booking;
import com.tripoo.model.Stop;

import java.util.List;

/**
 * Created by emil on 3/20/17.
 */
public interface StopService {

    List<Stop> findAll(int page);

    Stop findOne(Long id);

    Stop save(Stop stop);

    void delete(Long id);

    Stop update(Long id, Stop stop);

    void book(Long stopId, Booking booking);

}
