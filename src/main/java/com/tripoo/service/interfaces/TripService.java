package com.tripoo.service.interfaces;

import com.tripoo.model.Trip;

import java.util.List;

/**
 * Created by emil on 11/10/16.
 */
public interface TripService {
//    Iterable<Trip> findAll();

    List findAll(int page);

    Trip findOne(Long id);

    Trip update(Long id, Trip trip);

    List findWithParams(String startPoint, String endPoint, Long date, int page);

    Trip save(Trip trip);

    Trip saveTripToUser(Trip trip);

    void delete(Long id);

    Trip addCarToTrip(Long tripId, Long carId);

    void alertChecker(Trip trip);
}
