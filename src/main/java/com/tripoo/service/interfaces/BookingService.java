package com.tripoo.service.interfaces;

import com.tripoo.model.Booking;
import com.tripoo.model.Review;

/**
 * Created by emil on 12/5/16.
 */
public interface BookingService {
    Iterable<Booking> findAll();

    Booking findOne(Long id);

    Booking save(Booking booking);

    void delete(Long id);

    Booking update(Long id, Booking booking);

    Review addReview(Long id, Review review);
}

