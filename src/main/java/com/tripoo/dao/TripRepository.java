package com.tripoo.dao;

import com.tripoo.model.Stop;
import com.tripoo.model.Trip;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by emil on 11/7/16.
 */
@Repository
@Transactional
public interface TripRepository extends JpaRepository<Trip, Long> {

//    @Query("SELECT u FROM users u WHERE  u.stops.")
    List<Trip> findTripsByPrice(int price);
}

