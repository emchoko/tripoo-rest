package com.tripoo.dao;

import com.tripoo.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by emil on 12/6/16.
 */
@Repository
@Transactional
public interface CarRepository extends JpaRepository<Car, Long> {}