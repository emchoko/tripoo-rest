package com.tripoo.dao;

import com.tripoo.model.security.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by emil on 11/18/16.
 */
@Repository
@Transactional
public interface AuthorityRepository extends JpaRepository<Authority, Long> {
}
