package com.tripoo.dao;

import com.tripoo.model.Stop;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by emil on 3/20/17.
 */
public interface StopRepository extends JpaRepository<Stop, Long> {
}
