package com.tripoo.dao;

import com.tripoo.model.Alert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by emil on 12/28/16.
 */
@Repository
@Transactional
public interface AlertRepository extends JpaRepository<Alert, Long> {}
