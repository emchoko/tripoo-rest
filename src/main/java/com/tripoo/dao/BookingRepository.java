package com.tripoo.dao;

import com.tripoo.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by emil on 12/5/16.
 */
@Repository
@Transactional
public interface BookingRepository extends JpaRepository<Booking, Long> {
}
