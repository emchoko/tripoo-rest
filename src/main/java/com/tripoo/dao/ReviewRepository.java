package com.tripoo.dao;

import com.tripoo.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by emil on 11/8/16.
 */
@Repository
@Transactional
public interface ReviewRepository extends JpaRepository<Review, Long> {
}
