package com.tripoo.dto;

import com.tripoo.dto.list.UserListDTO;

/**
 * Created by emil on 12/25/16.
 */
public class ReviewDTO {

    private Long id;

    private String description;

    private int rating;

    private UserListDTO user;

    public ReviewDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public UserListDTO getUser() {
        return user;
    }

    public void setUser(UserListDTO user) {
        this.user = user;
    }
}
