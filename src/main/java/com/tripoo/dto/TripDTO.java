package com.tripoo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tripoo.model.Luggage;

import java.util.Date;
import java.util.List;

/**
 * Created by emil on 12/11/16.
 */

public class TripDTO {

    private Long id;

    private List<StopDTO> stops;

    private Luggage luggage;

    private String tripComment;

    private CarDTO car;

    private UserDTO user;

    @JsonFormat(pattern = "dd MMM yyyy")
    private Date date;

    private int price;

    TripDTO(){}

    public List<StopDTO> getStops() {
        return stops;
    }

    public void setStops(List<StopDTO> stops) {
        this.stops = stops;
    }

    public Luggage getLuggage() {
        return luggage;
    }

    public void setLuggage(Luggage luggage) {
        this.luggage = luggage;
    }

    public String getTripComment() {
        return tripComment;
    }

    public void setTripComment(String tripComment) {
        this.tripComment = tripComment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public CarDTO getCar() {
        return car;
    }

    public void setCar(CarDTO car) {
        this.car = car;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
