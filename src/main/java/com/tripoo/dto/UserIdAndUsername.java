package com.tripoo.dto;

/**
 * Created by emil on 1/22/17.
 */
public class UserIdAndUsername {

    private Long id;
    private String username;

    public UserIdAndUsername() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
