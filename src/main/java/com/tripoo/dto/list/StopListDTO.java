package com.tripoo.dto.list;

/**
 * Created by emil on 12/18/16.
 */
public class StopListDTO {

    private String name;

    public StopListDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
