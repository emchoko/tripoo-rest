package com.tripoo.dto.list;

import java.util.Date;

/**
 * Created by emil on 12/29/16.
 */
public class AlertListDTO {

    private Long id;

    private String startPoint;

    private String endPoint;

    private Date date;

    public AlertListDTO(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(String startPoint) {
        this.startPoint = startPoint;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
