package com.tripoo.dto.list;

/**
 * Created by emil on 2/14/17.
 */
public class BookingListDTO {

    private Long id;
    private Long userId;
    private StopListDTO route;

    public StopListDTO getRoute() {
        return route;
    }

    public void setRoute(StopListDTO route) {
        this.route = route;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

}
