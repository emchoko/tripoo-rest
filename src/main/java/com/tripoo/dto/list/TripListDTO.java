package com.tripoo.dto.list;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

/**
 * Created by emil on 12/18/16.
 */
public class TripListDTO {

    private Long id;

    @JsonFormat(pattern = "dd MMM yyyy")
    private Date date;

    private List<StopListDTO> stops;

    private int price;

    private UserListDTO user;

    public TripListDTO() {
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<StopListDTO> getStops() {
        return stops;
    }

    public void setStops(List<StopListDTO> stops) {
        this.stops = stops;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public UserListDTO getUser() {
        return user;
    }

    public void setUser(UserListDTO user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
