package com.tripoo.dto.list;

/**
 * Created by emil on 12/25/16.
 */
public class ReviewListDTO {

    private Long id;

    private String description;

    private int rating;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
