package com.tripoo.dto.all;

import com.tripoo.dto.BookingDTO;
import com.tripoo.dto.CarDTO;
import com.tripoo.dto.TripDTO;
import com.tripoo.dto.list.AlertListDTO;
import com.tripoo.model.Trip;
import com.tripoo.model.security.Authority;

import java.util.Date;
import java.util.List;

/**
 * Created by emil on 1/24/17.
 */
public class UserAll {

    private Integer id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private Date birthDate;
    private Boolean enabled;
    private Date lastPasswordResetDate;
    private List<TripDTO> trips = null;
    private String linkToFacebook;
    private String phoneNumber;
    private BookingDTO bookings;
    private List<AlertListDTO> alerts;
    private Double rating;
    private List<Authority> authorities;
    private List<CarDTO> cars;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public void setLastPasswordResetDate(Date lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    public List<TripDTO> getTrips() {
        return trips;
    }

    public void setTrips(List<TripDTO> trips) {
        this.trips = trips;
    }

    public String getLinkToFacebook() {
        return linkToFacebook;
    }

    public void setLinkToFacebook(String linkToFacebook) {
        this.linkToFacebook = linkToFacebook;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public BookingDTO getBookings() {
        return bookings;
    }

    public void setBookings(BookingDTO bookings) {
        this.bookings = bookings;
    }

    public List<AlertListDTO> getAlerts() {
        return alerts;
    }

    public void setAlerts(List<AlertListDTO> alerts) {
        this.alerts = alerts;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    public List<CarDTO> getCars() {
        return cars;
    }

    public void setCars(List<CarDTO> cars) {
        this.cars = cars;
    }
}
