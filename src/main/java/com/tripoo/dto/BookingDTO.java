package com.tripoo.dto;

/**
 * Created by emil on 1/24/17.
 */
public class BookingDTO {

    private Long id;
    private Long userId;
    private StopDTO route;
    private ReviewDTO review;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public StopDTO getRoute() {
        return route;
    }

    public void setRoute(StopDTO route) {
        this.route = route;
    }

    public ReviewDTO getReview() {
        return review;
    }

    public void setReview(ReviewDTO review) {
        this.review = review;
    }
}
