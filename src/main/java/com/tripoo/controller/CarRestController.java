package com.tripoo.controller;

import com.tripoo.model.Car;
import com.tripoo.service.interfaces.CarService;
import com.tripoo.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by emil on 12/7/16.
 */
@RestController
@RequestMapping("/cars")
public class CarRestController {

    @Autowired
    private CarService carService;

    @Autowired
    private Mapper mapper;

    /**
     * Get all cars
     * @param mapping - car_list, car_profile, car_all
     * @return - mapped car
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getCars(@RequestParam(value = "mapping") String mapping) {
        return ResponseEntity.ok().body(mapper.mapCars((List<Car>) carService.findAll(), mapping));
    }

    /**
     * Get car with id
     * @param id - id of the car
     * @param mapping - car_list, car_all
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity getCarById(@PathVariable Long id,
                                     @RequestParam(value = "mapping") String mapping) {
        return ResponseEntity.ok().body(mapper.mapCar(carService.findOne(id), mapping));
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addCar(@RequestBody Car car) {
        return ResponseEntity.ok().body(carService.save(car));
    }

    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity updateCarById(@PathVariable Long id,
                                        @RequestBody Car car) {
        return ResponseEntity.ok().body(carService.update(id, car));
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity deleteCar(@PathVariable Long id) {
        carService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
