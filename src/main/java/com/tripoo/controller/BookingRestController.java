package com.tripoo.controller;

import com.tripoo.model.Booking;
import com.tripoo.model.Review;
import com.tripoo.service.interfaces.BookingService;
import com.tripoo.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by emil on 12/5/16.
 */
@RestController
@RequestMapping("/bookings")
public class BookingRestController {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private Mapper mapper;

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity getBooking(@PathVariable Long id,
                                     @RequestParam("mapping") String mapping) {
        return ResponseEntity.ok().body(mapper.mapBookings(bookingService.findOne(id), mapping));
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity deleteBooking(@PathVariable Long id) {
        bookingService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @RequestMapping(method = RequestMethod.POST, value = "/{id}/reviews")
    public ResponseEntity addReviewToBooking(@PathVariable Long id,
                                             @RequestBody Review review) {
        return ResponseEntity.ok().body(bookingService.addReview(id, review));
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity updateBooking(@PathVariable Long id,
                                        @RequestBody Booking booking) {
        return ResponseEntity.ok().body(bookingService.update(id, booking));
    }
}
