package com.tripoo.controller;

import com.tripoo.model.User;
import com.tripoo.service.interfaces.CarService;
import com.tripoo.service.interfaces.UserService;
import com.tripoo.util.Mapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by emil on 11/8/16.
 */
@RestController
@RequestMapping("/users")
public class UserRestController {

    private static final String DEFAULT_USER_STRING_VALUE = "DEFAULT_USER_STRING_VALUE";

    private static final Logger logger = Logger.getLogger(UserRestController.class.getName());

    @Autowired
    private UserService userService;

    @Autowired
    private Mapper mapper;

    @Autowired
    private CarService carService;


    /**
     * Finds user with id equal to the given
     *
     * @param id      - user_id
     * @param mapping - type of mapping (two possibilities: user_list or user_profile)
     * @return mapped user
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity getUserById(@PathVariable Long id,
                                      @RequestParam(value = "mapping") String mapping) {
        return ResponseEntity.ok().body(mapper.mapUser(userService.findOne(id), mapping));
    }


    /**
     * Finds all users or user by username if param is available
     *
     * @param username - finds user by username
     * @return ResponseEntity
     */
    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    ResponseEntity getAllUsers(@RequestParam(value = "username", defaultValue = DEFAULT_USER_STRING_VALUE, required = false) String username,
                               @RequestParam(value = "mapping") String mapping) {

        if (username.equals(DEFAULT_USER_STRING_VALUE)) {
            return ResponseEntity.ok().body(mapper.mapUsers((List<User>) userService.findAll(), mapping));
        } else {
            return ResponseEntity.ok().body(mapper.mapUser(userService.findByUsername(username), mapping));
        }
    }


    /**
     * Get user trips
     * @param id - user id
     * @param date - after which date to return trips
     * @param mapping - possibilities: trip_list, trip_all
     * @return - List of mapped trips
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}/trips")
    public ResponseEntity getUserTrips(@PathVariable Long id,
                                       @RequestParam(value = "date", required = false) Long date,
                                       @RequestParam(value = "mapping") String mapping) {
        return ResponseEntity.ok().body(mapper.mapTrips(userService.findUserTrips(id, date), mapping));
    }


    /**
     * Get Bookings for user id
     *
     * @param userId  - id of user
     * @param mapping - show if to the returned info to be mapped
     *                possibilities: trips_list, trip_all, user_bookings
     * @return - JSON formatted response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/bookings")
    public ResponseEntity getUserBookingsFromId(@PathVariable Long userId,
                                                @RequestParam(value = "mapping", defaultValue = DEFAULT_USER_STRING_VALUE) String mapping) {
        switch (mapping) {
            case DEFAULT_USER_STRING_VALUE:
                return ResponseEntity.ok().body("Parameter \"mapping\" required");
            case "trip_list":
                return ResponseEntity.ok().body(mapper.mapTrips(userService.getUserBookedTrips(userId), mapping));
            case "user_bookings":
                return ResponseEntity.ok().body(mapper.mapBookings(userService.findOne(userId).getBookings(), mapping));
            case "user_list_bookings":
                return ResponseEntity.ok().body(mapper.mapBookings(userService.findOne(userId).getBookings(), mapping));
            default:
                return ResponseEntity.status(422).body("Parameter not known");
        }

    }

    /**
     * Get cars from user id
     * @param userId - id of user
     * @param mapping - possibilities: car_list, car_all
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/cars")
    public ResponseEntity getCarFromUserId(@PathVariable Long userId,
                                         @RequestParam(value = "mapping") String mapping) {
        return ResponseEntity.ok().body(mapper.mapCars(userService.findOne(userId).getCars(), mapping));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}/alerts")
    public ResponseEntity getAlertsFromUserId(@PathVariable Long userId,
                                              @RequestParam(value = "mapping") String mapping){
        return ResponseEntity.ok().body(mapper.mapAlerts(userService.findOne(userId).getAlerts(), mapping));
    }


    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @RequestMapping(method = RequestMethod.PATCH, value = "/{userId}/photoUrl")
    public ResponseEntity updatePhotoUrl(@PathVariable Long userId,
                                       @RequestBody User user) {
        userService.updatePhotoUrl(userId, user.getPhotoUrl());
        return ResponseEntity.ok().body("Updated!");
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @RequestMapping(method = RequestMethod.PATCH, value = "/{userId}/token")
    public ResponseEntity updateNotificationToken(@PathVariable Long userId,
                                         @RequestBody User user) {
        userService.updateToken(userId, user.getToken());
        return ResponseEntity.ok().body("Updated!");
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> addUser(@RequestBody User user) {
        return ResponseEntity.ok().body(userService.save(user));
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateUser(@PathVariable Long id,
                                     @RequestBody User user) {
        userService.update(id, user);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity deleteUser(@PathVariable Long id) {
        userService.delete(id);
        return ResponseEntity.ok().body("ADMIN deleted user with ID: " + id);
    }

}
