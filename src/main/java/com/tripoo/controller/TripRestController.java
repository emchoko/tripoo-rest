package com.tripoo.controller;

import com.tripoo.dao.TripRepository;
import com.tripoo.model.Stop;
import com.tripoo.model.Trip;
import com.tripoo.service.interfaces.TripService;
import com.tripoo.util.Mapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by emil on 11/4/16.
 */
@RestController
@RequestMapping("/trips")
public class TripRestController {

    private static final String DEFAULT_TRIP_STRING_VALUE = "DEFAULT_TRIP_STRING_VALUE";
    private static Logger logger = Logger.getLogger(TripRestController.class.getName());

    @Autowired
    private TripService tripService;

//    @Autowired
//    private RouteService routeService;

    @Autowired
    private Mapper mapper;
//
//    @RequestMapping(method = RequestMethod.GET, value = "/all")
//    public ResponseEntity getAllTripsByPage(@RequestParam(value = "page", required = true) int page) {
//        return ResponseEntity.ok().body(mapper.mapTrips(tripService.findAll(page), "trip_profile"));
//    }

    /**
     * Returns all trips if params are null
     *
     * @param startPoint - not required
     * @param endPoint   - not required
     * @param date       - not required
     * @param mapping    - required (trip_list, trip_all)
     * @return list of trips
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllTrips(@RequestParam(name = "page") int page,
                                      @RequestParam(value = "startPoint", required = false) String startPoint,
                                      @RequestParam(value = "endPoint", required = false) String endPoint,
                                      @RequestParam(value = "date", required = false, defaultValue = "0") Long date,
                                      @RequestParam(value = "mapping") String mapping) {

        return ResponseEntity.ok().body(mapper.mapTrips(tripService.findWithParams(startPoint, endPoint, date, page), mapping));
    }

    //TODO: fix this or remove it; might be redundant
    /**
     * Returns routes for a given trip with id
     *
     * @param tripId  - trip id
     * @param mapping - type of mapping (route_list, route_all)
     * @return - list of routes
     */
//    @RequestMapping(method = RequestMethod.GET, value = "/{tripId}/routes")
//    public ResponseEntity getTripRoutes(@PathVariable Long tripId,
//                                        @RequestParam(value = "mapping") String mapping) {
//        return ResponseEntity.ok().body(mapper.mapStops(tripService.findOne(tripId).getStops(), mapping));
//    }


    @Autowired
    private TripRepository tripRepo;

    @RequestMapping(method = RequestMethod.GET, value = "/byPoint")
    public ResponseEntity getTripsByPoint(@RequestParam(name = "point") int point){
//        Stop stop = new Stop();
//        stop.setName(point);

        return ResponseEntity.ok(tripRepo.findTripsByPrice(point));
    }


    /**
     * Return trip with given id
     *
     * @param id      - id of the trip
     * @param mapping - trip_list, trip_profile, trip_all
     * @return trip
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity getTripById(@PathVariable Long id,
                                      @RequestParam(value = "mapping") String mapping) {
        return ResponseEntity.ok().body(mapper.mapTrip(tripService.findOne(id), mapping));
    }

    //    TODO: Ask if this is RESTful enough

    /**
     * Add car to a trip
     *
     * @param tripId - id of the trip to add car to
     * @param carId  - id of the car
     * @return - returns the trip
     */
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @RequestMapping(method = RequestMethod.POST, value = "/{tripId}/cars/{carId}")
    public ResponseEntity addCarToTrip(@PathVariable Long tripId,
                                       @PathVariable Long carId) {
        return ResponseEntity.ok().body(tripService.addCarToTrip(tripId, carId));
    }


    //TODO: redundant? if not put it in the service
//    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
//    @RequestMapping(method = RequestMethod.POST, value = "/{tripId}/routes")
//    public ResponseEntity addRouteToTrip(@PathVariable Long tripId,
//                                         @RequestBody Route route) {
//        return ResponseEntity.ok(routeService.saveRouteToTrip(tripId, route));
//    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Trip> addTrip(@RequestBody Trip trip) {
        return ResponseEntity.ok(tripService.saveTripToUser(trip));
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTrip(@PathVariable Long id) {
        tripService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateTrip(@PathVariable Long id,
                                     @RequestBody Trip trip) {

        tripService.update(id, trip);
        return ResponseEntity.noContent().build();
    }

}
