package com.tripoo.controller;

import com.tripoo.model.Alert;
import com.tripoo.service.interfaces.AlertService;
import com.tripoo.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by emil on 12/29/16.
 */
@RestController
@RequestMapping("/alerts")
public class AlertRestController {

    @Autowired
    private AlertService alertService;

    @Autowired
    private Mapper mapper;

    /**
     * Get alert by id
     *
     * @param id      - id of alert
     * @param mapping - alert_list
     * @return - alert
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getAlertById(@PathVariable Long id,
                                       @RequestParam(value = "mapping") String mapping) {
        return ResponseEntity.ok().body(mapper.mapAlert(alertService.findOne(id), mapping));
    }

    /**
     * Retrieve all alerts
     *
     * @param mapping - alert_list, alert_all
     * @return list of alerts
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllAlerts(@RequestParam(value = "mapping") String mapping) {
        return ResponseEntity.ok().body(mapper.mapAlerts(alertService.findAll(), mapping));
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity createAlert(@RequestBody Alert alert) {
        return ResponseEntity.ok().body(alertService.save(alert));
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteTripById(@PathVariable Long id) {
        alertService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@PathVariable Long id,
                                 @RequestBody Alert alert) {
        return ResponseEntity.ok().body(alertService.update(id, alert));
    }


}
