package com.tripoo.controller;

import com.tripoo.model.Review;
import com.tripoo.service.interfaces.ReviewService;
import com.tripoo.util.Mapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by emil on 11/8/16.
 */
@RestController
@RequestMapping("/reviews")
public class ReviewRestController {

    private static Logger logger = Logger.getLogger(ReviewRestController.class.getName());

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private Mapper mapper;

    /**
     * Get review by id
     * @param id - id of review
     * @param mapping - review_list, review_profile, review_all
     * @return review
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Review> getReviewById(@PathVariable Long id,
                                                @RequestParam(value = "mapping") String mapping) {
        return ResponseEntity.ok().body(mapper.mapReview(reviewService.findOne(id), mapping));
    }

    /**
     * Get all reviews
     * @param mapping - review_list, review_all
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Review>> getAllReviews(@RequestParam(value = "mapping") String mapping) {
        return ResponseEntity.ok().body(mapper.mapReviews((List<Review>) reviewService.findAll(), mapping));
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Review> addReview(@RequestBody Review review) {
        return ResponseEntity.ok().body(reviewService.save(review));
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Review> updateReview(@PathVariable Long id, @RequestBody Review review) {
        return ResponseEntity.ok().body(reviewService.update(id, review));
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteReview(@PathVariable Long id) {
        logger.debug("DELETE REVIEW!");
        reviewService.delete(id);
        return ResponseEntity.ok().build();
    }

}
