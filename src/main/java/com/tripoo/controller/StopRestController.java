package com.tripoo.controller;

import com.tripoo.model.Booking;
import com.tripoo.model.Stop;
import com.tripoo.service.interfaces.StopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by emil on 11/29/16.
 */
@RestController
@RequestMapping("/stops")
public class StopRestController {

    @Autowired
    private StopService stopService;

//    @Autowired
//    private Mapper mapper;

    /**
     * Return all the routes
     *
     * @param mapping - route_all, route_profile, route_list
     * @return - all routes
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllRoutes(@RequestParam(value = "mapping") String mapping,
                                        @RequestParam(value = "page") int page) {
//        return ResponseEntity.ok().body(mapper.mapStops(stopService.findAll(page), mapping));
        //TODO: make the mapper for stop
        return null;
    }

    /**
     * Returns mapped route with id
     *
     * @param id      - id of the route
     * @param mapping - route_all, route_profile, route_list
     * @return - route
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity getStopById(@PathVariable Long id,
                                      @RequestParam(value = "mapping") String mapping) {
//        return ResponseEntity.ok().body(mapper.mapStop(stopService.findOne(id), mapping));
        //TODO: make the mapper for stop
        return null;
    }

//    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
//    @RequestMapping(method = RequestMethod.POST)
//    public ResponseEntity createRoute(@RequestBody Stop stop) {
//        return ResponseEntity.ok().body(stopService.save(route));
//    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @RequestMapping(method = RequestMethod.POST, value = "/{stopId}/book")
    public ResponseEntity bookStop(@PathVariable Long stopId,
                                   @RequestBody Booking booking) {
        stopService.book(stopId, booking);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity updateRoute(@RequestBody Stop stop,
                                      @PathVariable Long id) {
        return ResponseEntity.ok().body(stopService.update(id, stop));
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity deleteRoute(@PathVariable Long id) {
        stopService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
