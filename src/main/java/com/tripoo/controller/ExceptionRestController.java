package com.tripoo.controller;

import com.tripoo.exceptions.ErrorDetail;
import com.tripoo.exceptions.NotMatchingUserException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Created by emil on 12/7/16.
 */
@ControllerAdvice
public class ExceptionRestController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NotMatchingUserException.class)
    public ResponseEntity myError(Exception exception) {
        ErrorDetail error = new ErrorDetail();
        error.setStatus(HttpStatus.BAD_REQUEST.value());
        error.setMessage(exception.getLocalizedMessage());
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED.value()).body(error);
    }
}
