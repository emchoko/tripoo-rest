package com.tripoo.util;

import com.tripoo.dto.*;
import com.tripoo.dto.all.UserAll;
import com.tripoo.dto.list.*;
import com.tripoo.exceptions.NotKnownParameter;
import com.tripoo.model.*;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by emil on 12/23/16.
 */

/**
 * Mapper - maps users, trips
 */
@SuppressWarnings("unchecked")
@Service
public class Mapper {

    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    private Map<String, Class<?>> mappingToClass = new HashMap<>();

    @PostConstruct
    void init() {
        mappingToClass = new HashMap<>();

        mappingToClass.put(Constants.USER_LIST, UserListDTO.class);
        mappingToClass.put(Constants.USER_PROFILE, UserProfileDTO.class);
        mappingToClass.put(Constants.USER_BY_USERNAME, UserIdAndUsername.class);
        mappingToClass.put(Constants.USER_ALL, UserAll.class);

        mappingToClass.put(Constants.TRIP_LIST, TripListDTO.class);
        mappingToClass.put(Constants.TRIP_PROFILE, TripDTO.class);

        mappingToClass.put(Constants.STOP_LIST, StopListDTO.class);
        mappingToClass.put(Constants.STOP_PROFILE, StopDTO.class);

        mappingToClass.put(Constants.CAR_PROFILE, CarDTO.class);

        mappingToClass.put(Constants.REVIEW_LIST, ReviewListDTO.class);
        mappingToClass.put(Constants.REVIEW_PROFILE, ReviewDTO.class);

        mappingToClass.put(Constants.ALERT_LIST, AlertListDTO.class);

        mappingToClass.put(Constants.BOOKING_DTO, BookingDTO.class);
        mappingToClass.put(Constants.BOOKING_LIST_DTO, BookingListDTO.class);
    }

    public <T> T mapUser(User user, String mapping) {
        return dozerBeanMapper.map(user,
                (Class<T>) Optional
                        .ofNullable(mappingToClass.get(mapping))
                        .orElseThrow(() -> new NotKnownParameter("Parameter:" + mapping + " not known")));
    }

    public <T> List<T> mapUsers(List<User> users, String mapping) {
        return users.stream()
                .map(user ->
                        dozerBeanMapper.map(user,
                                (Class<T>) Optional
                                        .ofNullable(mappingToClass.get(mapping))
                                        .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known"))))
                .collect(Collectors.toList());
    }

    public <T> T mapTrip(Trip trip, String mapping) {
        return dozerBeanMapper.map(trip,
                (Class<T>) Optional
                        .ofNullable(mappingToClass.get(mapping))
                        .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known")));
    }

    public <T> List<T> mapTrips(List<Trip> trips, String mapping) {
        return trips.stream()
                .map(trip -> dozerBeanMapper.map(trip,
                        (Class<T>) Optional
                                .ofNullable(mappingToClass.get(mapping))
                                .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known"))))
                .collect(Collectors.toList());
    }

    public <T> T mapStop(Stop stop, String mapping) {
        return dozerBeanMapper.map(stop,
                (Class<T>) Optional.
                        ofNullable(mappingToClass.get(mapping))
                        .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known")));
    }

    public <T> List<T> mapStops(List<Stop> stops, String mapping) {
        return stops.stream()
                .map(route -> dozerBeanMapper.map(route,
                        (Class<T>) Optional
                                .ofNullable(mappingToClass.get(mapping))
                                .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known"))))
                .collect(Collectors.toList());
    }

    public <T> T mapCar(Car car, String mapping) {
        return dozerBeanMapper.map(car,
                (Class<T>) Optional
                        .ofNullable(mappingToClass.get(mapping))
                        .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known")));
    }

    public <T> List<T> mapCars(List<Car> cars, String mapping) {
        return cars.stream()
                .map(car -> dozerBeanMapper.map(car,
                        (Class<T>) Optional
                                .ofNullable(mappingToClass.get(mapping))
                                .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known"))))
                .collect(Collectors.toList());
    }

    public <T> T mapReview(Review review, String mapping) {
        return dozerBeanMapper.map(review,
                (Class<T>) Optional
                        .ofNullable(mappingToClass.get(mapping))
                        .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known")));
    }

    public <T> List<T> mapReviews(List<Review> reviews, String mapping) {
        return reviews.stream()
                .map(review -> dozerBeanMapper.map(review,
                        (Class<T>) Optional
                                .ofNullable(mappingToClass.get(mapping))
                                .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known"))))
                .collect(Collectors.toList());
    }

    public <T> T mapAlert(Alert alert, String mapping) {
        return dozerBeanMapper.map(alert,
                (Class<T>) Optional
                        .ofNullable(mappingToClass.get(mapping))
                        .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known")));
    }

    public <T> List<T> mapAlerts(List<Alert> alerts, String mapping) {
        return alerts.stream()
                .map(alert -> dozerBeanMapper.map(alert,
                        (Class<T>) Optional
                                .ofNullable(mappingToClass.get(mapping))
                                .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known"))))
                .collect(Collectors.toList());
    }

    public <T> List<T> mapBookings(List<Booking> bookings, String mapping) {
        return bookings.stream()
                .map(booking -> dozerBeanMapper.map(booking,
                        (Class<T>) Optional
                                .ofNullable(mappingToClass.get(mapping))
                                .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known")))).collect(Collectors.toList());
    }

    public <T> T mapBookings(Booking one, String mapping) {
        return dozerBeanMapper.map(one,
                (Class<T>) Optional
                        .ofNullable(mappingToClass.get(mapping))
                        .orElseThrow(() -> new NotKnownParameter("Parameter:\"" + mapping + "\" not known")));
    }
}
