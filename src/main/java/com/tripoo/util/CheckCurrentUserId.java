package com.tripoo.util;

import com.tripoo.model.security.JwtUser;

import java.util.Objects;

/**
 * Created by emil on 12/6/16.
 */
public class CheckCurrentUserId {

    public static boolean check(Long userId) {
        JwtUser user = UserContext.getUser();
        assert user != null;
        return Objects.equals(user.getId(), userId);

    }
}
