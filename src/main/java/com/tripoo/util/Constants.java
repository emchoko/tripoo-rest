package com.tripoo.util;

/**
 * Created by emil on 1/24/17.
 */
public interface Constants {
    String USER_LIST = "user_list";
    String USER_PROFILE = "user_profile";
    String USER_BY_USERNAME = "user_by_username";
    String USER_ALL = "user_all";

    String TRIP_LIST = "trip_list";
    String TRIP_PROFILE = "trip_profile";

//    String ROUTE_LIST = "route_list";
//    String ROUTE_PROFILE = "route_profile";

    String STOP_LIST = "stop_list";
    String STOP_PROFILE = "stop_profile";

    String CAR_PROFILE = "car_profile";

    String REVIEW_PROFILE = "review_profile";
    String REVIEW_LIST = "review_list";

    String ALERT_LIST = "alert_list";

    String FIREBASE_SERVER_KEY = "key=AAAAosnXGYY:APA91bHR4DHA11BLd5ogOu4TI5O1q0TVfUbS--22-kqQfwpwlBssC0_M407FMQKbF-SSxBz8TPx7PuWrXUVeaW_y3qgYrUnPnhDBc6-qZ3Qj_8n9xmlFVym01D2CoczRqsc0Z1vWMLLw";
    String BOOKING_DTO = "user_bookings";
    String BOOKING_LIST_DTO = "user_list_bookings";
    int DEFAULT_PAGE_SIZE = 10;
}
