package com.tripoo.util;

import com.tripoo.model.security.JwtUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by emil on 11/22/16.
 */
public class UserContext {

    public static JwtUser getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            return (JwtUser) authentication.getPrincipal();
        }
        return null;
    }
}
